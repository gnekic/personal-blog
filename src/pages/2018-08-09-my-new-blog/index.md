---
title: Welcome to My New Blog
date: "2018-08-09"
path: "/my-new-blog/"
---

Last week I decided to finally transition my current blog over to GatsbyJS. Because I wanted to generally just revamp the blog both content-wise and performance-wise.

<!-- end -->

In this post I want to briefly touch on the reasoning behind my move to Gatsby.

## Reasoning

Learning, Security, and Performance.

### Learning

Learning was arguably one of my biggest reasons for making the move. I always enjoy experimenting with new frameworks and technologies in an effort to improve my overall workflow. That, paired with the fact that I'm a huge fan of React in general.

### Security

...one word Wordpress.

### Performance

Well it is fast :)

_TL;DR: Gatsby is pretty awesome, give it a try._