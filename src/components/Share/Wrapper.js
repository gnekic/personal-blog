import styled from 'styled-components';
import { borderColour } from '../../variables';

const Wrapper = styled.div`
  border: 1px solid ${borderColour};
  margin: -50px 30px 120px;
  padding: 15px;
  text-align: center;
`;

export default Wrapper;