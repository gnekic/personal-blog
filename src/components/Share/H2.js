import styled from 'styled-components';
import { textColour } from '../../variables';

const H2 = styled.h2`
  color: ${textColour};
  font-size: 1.2em;
  margin: 0 0 5px;
`;

export default H2;