import styled from 'styled-components';
import { borderColour } from '../../variables';

const Wrapper = styled.div`
  border-bottom: 1px solid ${borderColour};
  padding: 75px 0;
`;

export default Wrapper;