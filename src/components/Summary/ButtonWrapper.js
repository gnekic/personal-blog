import styled from 'styled-components';

const ButtonWrapper = styled.div`
  text-align: right;
`;

export default ButtonWrapper;