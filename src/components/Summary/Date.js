import styled from 'styled-components';

const Date = styled.span`
  font-size: 0.85em;
`;

export default Date;