import styled from 'styled-components';

const Container = styled.div`
  margin: 0 auto;
  max-width: 700px;
  padding: 0 15px;
`;

export default Container;