import React from 'react';

import Link from './Link';
import GatsbyLink from './GatsbyLink';

function Nav() {
  return (
    <nav>
      <Link href="https://gordan.neki.ch/">Home</Link>
      <Link href="https://gordan.neki.ch/#projects">Projects</Link>
      <GatsbyLink to="/">Blog</GatsbyLink>
      <Link href="https://gordan.neki.ch/#contact">Contact</Link>
    </nav>
  );
}

export default Nav;