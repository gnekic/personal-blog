import React from 'react';

import Wrapper from './Wrapper';
import LogoWrapper from './LogoWrapper';
import NavWrapper from './NavWrapper';
import Logo from './logo.png';
import Nav from '../Nav';

function Header() {
  return (
    <Wrapper id="top">
      <LogoWrapper>
        <a href="https://gordan.neki.ch"><img src={Logo} alt="Gordan Nekić"/></a>
      </LogoWrapper>
      <NavWrapper>
        <Nav/>
      </NavWrapper>  
    </Wrapper>
  );
}

export default Header;
