import React from 'react';

import Wrapper from './Wrapper';
import H1 from './H1';

function SubHeader() {
  return (
    <Wrapper>
      <H1>Gordan Nekić</H1>
      <span>Development Blog</span>
    </Wrapper>
  );
}

export default SubHeader;
